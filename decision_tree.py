import numpy as np
from node import InternalNode, LeafNode

class DecisionTree:
    """
    Simple basic decision tree implemented using recursion.
    """
    def __init__(self, max_height=3):
        self._max_height = max_height

    def fit(self, X, Y):
        """
        Train the tree
        :param X: pandas.DataFrame
        :param Y: pandas.Series
        :return: None
        """
        self._X = X # Storing for ease of access.
        self._Y = Y
        self.root = self._create_node(1, X.index)
        del self._X # Remove after training.
        del self._Y

    def _create_node(self, height, indices):
        """
        Create tree nodes recursively
        :param height: height of the tree at this node
        :param indices: set of indices for this node
        :return: Node
        """
        if len(indices) == 0:
            return None

        if self._is_pure(indices) or height > self._max_height:
            return LeafNode(self._get_Y(indices))

        variable, threshold, left_indices, right_indices = self._split(indices)
        node = InternalNode(variable, threshold)
        node._left = self._create_node(height + 1, left_indices)
        node._right = self._create_node(height + 1, right_indices)

        return node

    def _get_Y(self, indices):
        """
        Get Y of the majority of points
        :param indices:
        :return: int Y category of the point set
        """
        values, counts = np.unique(self._Y.loc[indices].to_numpy(), return_counts=True)
        return values[np.argmax(counts)]

    def _is_pure(self, indices):
        """
        Set of points is pure if it contains Y's of only one kind
        :param indices: list of indices of points
        :return: boolean
        """
        len(self._Y.loc[indices].unique()) <= 1

    def _split(self, indices):
        """
        Split indices in two using gini criterion TODO add entropy option
        :param indices: set of indices to split
        :return: split variable and threshold, and also splitted indices
        """
        best_variable = ''
        best_threshold = 0
        best_left_indices = None
        best_right_indices = None
        best_gini = 1

        points = self._X.loc[indices]

        for variable_name in self._X.columns:
            variable = points[variable_name]
            for threshold in sorted(variable.unique()): # Split thresholds are just values of the variable
                left_indices = variable[variable <= threshold].index
                right_indices = variable[variable > threshold].index
                gini = self._gini_per_split(self._Y.loc[left_indices], self._Y.loc[right_indices])
                if gini < best_gini:
                    best_variable = variable_name
                    best_threshold = threshold
                    best_left_indices = left_indices
                    best_right_indices = right_indices
                    best_gini = gini

        return best_variable, best_threshold, best_left_indices, best_right_indices

    def _gini_per_split(self, left_Y, right_Y) -> float:
        """
        Calculate gini of left and right point set and return their weighted average weighted by
        number of points in the set
        :param left_Y: indices of left side of the split
        :param right_Y: indices of right side of the split
        :return: float weighted average of ginis
        """
        total = len(left_Y) + len(right_Y)
        left_freq = len(left_Y) / total
        right_freq = len(right_Y) / total

        return left_freq * self._gini(left_Y) + right_freq * self._gini(right_Y)

    def _gini(self, Y):
        counts = np.unique(Y.to_numpy(), return_counts=True)[1]
        norm_counts = counts / counts.sum()
        return 1 - (norm_counts**2).sum()

    def predict(self, X):
        """
        Predict Y given X
        :param X: pandas.DataFrame
        :return: pandas.Series predicted Y
        """
        return X.apply(self.root.predict, axis=1)

    def __str__(self):
        return str(self.root)