from decision_tree import DecisionTree
import pandas as pd

# Create training set
training_set = pd.read_csv('train.csv')
training_set = training_set[['Age', 'Fare', 'Survived']]
training_set.dropna(inplace=True)
training_X = training_set[['Age', 'Fare']]
training_Y = training_set['Survived']

# Train the tree
tree = DecisionTree(max_height=3)
tree.fit(training_X, training_Y)
print(tree)

# Predict on testing set
test_set = pd.read_csv('test.csv')
test_X = test_set[['Age', 'Fare']]
predictions = tree.predict(test_X)
test_set.insert(1, 'Survived', predictions)

# Create prediction for kaggle
result = test_set[['PassengerId', 'Survived']]
result.to_csv('prediction.csv', index=False)

print(f'Prediction: survived {sum(predictions)} out of {len(predictions)} passengers.')
