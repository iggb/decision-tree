# decision tree

Classification decision tree implementation using recursion.
The files test.csv and train.csv are from kaggle Titanic dataset.
To run:
```python3 main.py```
It creates file prediction.csv with the results that you can
submit to kaggle. Also it displays poor text representation of
the tree and prediction of how many people survived. I've tested
only with this dataset, with Age and Fare variables. I've tried
to keep the code as minimal and simple as possible so that
there's a chance that someone might actually read it.