class InternalNode:
    """
    Internal node of decision tree. It has left and right children.
    """
    def __init__(self, variable, threshold):
        self._variable = variable
        self._threshold = threshold
        self._left = None
        self._right = None

    def predict(self, X):
        """
        Route prediction to left or right child based on threshold
        :param X: pandas.Series -
        :return:
        """
        value = X[self._variable]
        if value <= self._threshold:
            return self._left.predict(X)
        return self._right.predict(X)

    def __str__(self, indent=0):
        result = f'{" " * indent}{self._variable} <= {self._threshold}\n'
        if self._left != None:
            result += self._left.__str__(indent=indent + 4)
        if self._right != None:
            result += self._right.__str__(indent=indent + 4)

        return result


class LeafNode:
    """
    Leaf node of decision tree. It's value is the Y that it predicts
    """
    def __init__(self, value):
        """
        :param value: int - Y value of the node
        """
        self._value = value

    def predict(self, _):
        return self._value

    def __str__(self, indent=0):
        return f'{" " * indent}{self._value}\n'
